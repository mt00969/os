package PSS;

import java.io.IOException;
import java.util.concurrent.Semaphore;

public class ThreadA extends Thread {

	String threadName;
	Method mA;
	static Semaphore sem;

	public static Semaphore getSemaphore() {
		return sem;
	}

	@SuppressWarnings("static-access")
	ThreadA(String threadName, Method m, Semaphore sem) {
		super();
		this.threadName = threadName;
		mA = m;
		this.sem = sem;
	}

	@Override
	public void run() {

		synchronized (mA) {
			mA.print(1, threadName);

			try {
				System.out.print(threadName + " is waiting for a permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");

				sem.acquire();
				System.out.print(threadName + " gets the permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");
				Main.fcfs();

				System.out.print(threadName + " releases the permit ");

				Main.Timer(50, true, ".");
				System.out.println("\n");
				sem.release();

			} catch (InterruptedException | NumberFormatException | IOException e) {
				e.printStackTrace();
			}

		}

	}
}
